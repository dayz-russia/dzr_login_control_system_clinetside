modded class MissionGameplay
{
	void MissionGameplay()
	{
		Print("[dzr_login_control_system_clientside_v2] ::: Starting Clientside");
		
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "GetPlayerRealName", this, SingleplayerExecutionType.Both );
		GetRPCManager().AddRPC( "DZR_LCS_RPC", "OpenMismatchURL", this, SingleplayerExecutionType.Both );
	};
	
	void OpenMismatchURL(CallType type, ref ParamsReadContext ctx, ref PlayerIdentity sender, ref Object target)
	{
		ref Param6<string, string, string, string, string, string> data;
		if ( !ctx.Read( data ) )
		{
			Print("OpenMismatchURL !ctx.Read( data )");
			return;	
		};
		if (type == CallType.Client)
        {
            if (data.param1)
            {
				/*
					string request_username;
					string returned_username;
					string status;
					string requested_steamid;
					string returned_steamid; 76561198042321129
				*/
				if(data.param3 == "NOT_REGISTERED" || data.param3 == "ERROR_STEAM_MISMATCH" || data.param3 == "REGISTERED_NO_STEAM_ID" || data.param3 == "ADMIN_BAN"  || data.param3 == "ADMIN_BAN2"  || data.param3 == "STEAMID_ALREADY_LINKED" )
				{
					string strSub = "";
					if(data.param5 != "")
					{
						strSub = data.param5.Substring(13, 4);
					};
					GetGame().OpenURL(data.param6+"&request_username="+data.param1+"&returned_username="+data.param2+"&status="+data.param3+"&requested_steamid="+data.param4+"&returned_steamid=*************"+strSub);
					//GetGame().OpenURL("https://dayzrussia.com/lpdzr/index.php?request_username="+data.param1+"&returned_username="+data.param2+"&status="+data.param3+"&requested_steamid="+data.param4+"&returned_steamid=*************"+strSub);
					GetGame().RequestExit(1);
				};
				
				if(data.param3 == "LINKED_STEAM_ID" || data.param3 == "LINKED_STEAM_ID2")
				{
					GetGame().OpenURL(data.param6+"&request_username="+data.param1+"&returned_username="+data.param2+"&status="+data.param3+"&requested_steamid="+data.param4+"&returned_steamid=*************"+strSub);
				};
			};
		};
	};
	
	
};	

